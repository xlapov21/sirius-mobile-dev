package com.example.myapplication.presentationlayer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.myapplication.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SimpleListFragment.newInstance())
            .commit()
    }

    fun onImageClick(view: View) {
        val imageUrl = view.tag as String // получаем url из тега ImageView
        val intent = Intent(this, FullImageActivity::class.java)
        intent.putExtra("image_url", imageUrl)
        startActivity(intent)
    }
}