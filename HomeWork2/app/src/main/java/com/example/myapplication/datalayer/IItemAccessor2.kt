package com.example.myapplication.datalayer

import com.example.myapplication.objects.Item
import retrofit2.http.GET

interface IItemAccessor2 {
	@GET("/v2/beers")
	suspend fun items2(): List<Item>
}