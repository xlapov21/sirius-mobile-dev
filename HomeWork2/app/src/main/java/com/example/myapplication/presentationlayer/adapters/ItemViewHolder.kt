package com.example.myapplication.presentationlayer.adapters

import android.graphics.drawable.Drawable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.example.myapplication.R
import com.example.myapplication.objects.Item
import com.bumptech.glide.request.target.Target


class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    protected val image = view.findViewById<ImageView>(R.id.image)

    fun bind(item: Item) {
        Glide.with(itemView).load(item.imageValue()).listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                var progress_bar = itemView.findViewById<ProgressBar>(R.id.progress_bar)
                progress_bar.visibility = GONE
                var button = itemView.findViewById<Button>(R.id.button)
                button.visibility = VISIBLE
                button.setOnClickListener {
                    button.visibility = GONE
                    progress_bar.visibility = VISIBLE
                    bind(item)
                }

                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {

                itemView.findViewById<ProgressBar>(R.id.progress_bar).visibility =
                    GONE
                itemView.findViewById<Button>(R.id.button).visibility = GONE

                return false
            }

        }).into(image)
    }
}