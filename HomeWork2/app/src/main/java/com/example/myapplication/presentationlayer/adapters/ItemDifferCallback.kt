package com.example.myapplication.presentationlayer.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.myapplication.objects.Item

class ItemDifferCallback : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem.image_url == newItem.image_url
    }

    override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
        return false
    }
}