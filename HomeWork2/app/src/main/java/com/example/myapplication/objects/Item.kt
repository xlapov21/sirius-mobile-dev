package com.example.myapplication.objects

import com.google.gson.annotations.SerializedName

open class Item {
    @SerializedName("image_url")
    var image_url = ""

    fun imageValue() = "$image_url"

}