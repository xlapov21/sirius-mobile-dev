package com.example.myapplication.presentationlayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.businesslayer.CoroutineItemProvider
import com.example.myapplication.datalayer.IItemAccessor2
import com.example.myapplication.datalayer.RetrofitProvider
import com.example.myapplication.presentationlayer.adapters.ItemAdapter

class SimpleListFragment : Fragment() {
    protected val provider by lazy { initializeProvider() }
    protected val itemAdapter = ItemAdapter()

    private val accessor = RetrofitProvider().provide().create(IItemAccessor2::class.java)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.content_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<RecyclerView>(R.id.list).apply {
            layoutManager = GridLayoutManager(requireContext(), COLUMN_COUNT)
            adapter = itemAdapter
        }

        provider.load {
            itemAdapter.submitList(it)
        }
    }

    private fun initializeProvider(): CoroutineItemProvider {
        return CoroutineItemProvider(accessor)
    }


    companion object {
        protected const val COLUMN_COUNT = 4

        fun newInstance(): SimpleListFragment {
            return SimpleListFragment()
        }
    }
}